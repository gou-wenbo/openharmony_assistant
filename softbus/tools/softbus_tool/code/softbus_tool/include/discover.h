/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DISCOVER_H
#define DISCOVER_H

#ifdef __cplusplus
extern "C" {
#endif

void D_GetBtMac(void);
void D_PublishLNN(void);
void D_StopPublishLNN(void);
void D_RefreshLNN(void);
void D_StopRefreshLNN(void);

#ifdef __cplusplus
}
#endif
#endif /* DISCOVER_H */
