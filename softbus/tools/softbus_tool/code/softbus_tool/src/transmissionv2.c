/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "transmissionv2.h"

#include "common.h"
#include "socket.h"
#include "softbus_common.h"

#define DATA_SIZE 128
static sem_t g_bindSem;

static char *g_sessionNameV2 = "com.huawei.dmsdp.softbus_tool_";

static const char *g_datadir = "/data";

static bool bindFlag = false;
void OnBind(int32_t socket, PeerSocketInfo info)
{
    printf("\n>>>Onbind socket = %d.", socket);
    printf("\n>>>Onbind peer socket info name= %s, networkId = %s.", info.name, info.networkId);
    printf("\n>>>Onbind peer socket info pkgName= %s, TransDataType = %d.\n", info.pkgName, info.dataType);
    Logf(INFO, "Call On bind success, socket fd = %d\n", socket);
    if (bindFlag) {
        sem_post(&g_bindSem);
    }
    return;
}

void OnShutdown(int32_t socket, ShutdownReason reason)
{
    printf("\n>>>OnShutdown socket = %d, shutdown reason = %d.\n", socket, reason);
    return;
}

void OnError(int32_t socket, int32_t errCode)
{
    printf("\n>>>OnError socket = %d, errCode = %d.\n", socket, errCode);
    Logf(INFO, "Call On bind failed, socket fd = %d\n", socket);
    sem_post(&g_bindSem);
    return;
}

void OnBytes(int32_t socket, const void *data, uint32_t dataLen)
{
    printf("\n>>>OnBytes socket = %d, data = %s.\n", socket, (const char *)data);
    return;
}

void OnMessage(int32_t socket, const void *data, uint32_t dataLen)
{
    printf("\n>>>OnMessage socket = %d, data = %s.\n", socket, (const char *)data);
    return;
}

void OnStream(int32_t socket, const StreamData *data, const StreamData *ext, const StreamFrameInfo *param)
{
    printf("\n>>>OnStream socket = %d, data = %s, ext = %s.\n", socket, data->buf, ext->buf);
    return;
}

static const char *UpdateRecvPath()
{
    return g_datadir;
}

void OnFile(int32_t socket, FileEvent *event)
{
    printf("\n>>>OnFile socket = %d, FileEventType = %d.", socket, event->type);
    int i;
    for (i = 0; i < event->fileCnt; i++) {
        printf("\n>>>OnFile filename id = %d, name = %s", i, event->files[i]);
    }
    printf("\n>>>OnFile bytesProcessed = %" PRId64 ", bytesTotal = %" PRId64 ".", event->bytesProcessed, event->bytesTotal);
    event->UpdateRecvPath = UpdateRecvPath;
    return;
}

void OnQos(int32_t socket, QoSEvent eventId, const QosTV *qos, uint32_t qosCount)
{
    printf("\n>>>OnQos socket = %d, QoSEvent = %d.", socket, eventId);
    printf("\n>>>OnQos qos.QosType = %d, qos.Value = %d, qosCount = %d.", qos->qos, qos->value, qosCount);
    return;
}

void T_Socket(void)
{
    printf("\n>>>Choose use socket function\n");
    char networkId[NETWORK_ID_BUF_LEN];
    GetInputString("Please input peeer network Id:", networkId, NETWORK_ID_BUF_LEN);
    char sessionNameSuffix[5];
    GetInputString("Please input session name suffix (max 5 chars):", sessionNameSuffix, 5);
    char sessionName[36] = {0};
    strcat_s(sessionName, 31, g_sessionNameV2);
    strcat_s(sessionName, 37, sessionNameSuffix);
    SocketInfo info;
    info.pkgName = PKG_NAME;
    info.name = sessionName;
    info.peerName = sessionName;
    info.peerNetworkId = networkId;
    info.dataType =
        GetInputNumber("Please input socket data type(1 - TYPE_MESSAGE, 2 - TYPE_BYTES, 3 - TYPE_FILE, 4 - "
                       "TYPE_RAW_STREAM, 5 - TYPE_VIDEO_STREAM, 6 - TYPE_AUDIO_STREAM, 7 - TYPE_SLICE_STREAM):");
    int32_t socket = Socket(info);
    if (socket > 0) {
        printf("Socket success, socket fd = %d\n", socket);
    } else {
        printf("Socket failed, errCode = %d\n", socket);
    }
}

static ISocketListener g_listener = {
    .OnBind = OnBind,
    .OnError = OnError,
    .OnShutdown = OnShutdown,
    .OnMessage = OnMessage,
    .OnStream = OnStream,
    .OnFile = OnFile,
    .OnQos = OnQos,
    .OnBytes = OnBytes,
};

void T_Bind(void)
{
    printf("\n>>>Choose use bind function\n");
    int32_t socketfd = GetInputNumber("Please input socket fd id:");
    int32_t isCustomQosInfo = GetInputNumber("Use custom qos info ? 0 - default (QOS_TYPE_MIN_BW=10*1024*1024, "
                                             "QOS_TYPE_MAX_WAIT_TIMEOUT=4000, QOS_TYPE_MIN_LATENCY=2000), 1 - custom :");
    QosTV qosInfoDefault[] = {
        {.qos = QOS_TYPE_MIN_BW,            .value = 10 * 1024 * 1024},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT, .value = 4000            },
        { .qos = QOS_TYPE_MIN_LATENCY,      .value = 2000            },
    };
    int32_t qosCount = 0;
    int32_t values[7];
    if (isCustomQosInfo == 1) {
        values[0] = GetInputNumber("Please input QOS_TYPE_MIN_BW value:");
        values[1] = GetInputNumber("Please input QOS_TYPE_MAX_WAIT_TIMEOUT value:");
        values[2] = GetInputNumber("Please input QOS_TYPE_MIN_LATENCY value:");
        values[3] = GetInputNumber("Please input QOS_TYPE_MAX_BUFFER value:");
        values[4] = GetInputNumber("Please input QOS_TYPE_FIRST_PACKAGE value:");
        values[5] = GetInputNumber("Please input QOS_TYPE_MAX_IDLE_TIMEOUT value:");
        values[6] = GetInputNumber("Please input QOS_TYPE_TRANS_RELIABILITY value:");
    }
    QosTV qosInfoCustorm[] = {
        {.qos = QOS_TYPE_MIN_BW,             .value = values[0]},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT,  .value = values[1]},
        { .qos = QOS_TYPE_MIN_LATENCY,       .value = values[2]},
        { .qos = QOS_TYPE_MAX_BUFFER,        .value = values[3]},
        { .qos = QOS_TYPE_FIRST_PACKAGE,     .value = values[4]},
        { .qos = QOS_TYPE_MAX_IDLE_TIMEOUT,  .value = values[5]},
        { .qos = QOS_TYPE_TRANS_RELIABILITY, .value = values[6]},
    };
    QosTV *qosInfo;
    if (isCustomQosInfo) {
        qosInfo = &qosInfoCustorm[0];
        qosCount = sizeof(qosInfoCustorm) / sizeof(qosInfoCustorm[0]);
    } else {
        qosInfo = &qosInfoDefault[0];
        qosCount = sizeof(qosInfoDefault) / sizeof(qosInfoDefault[0]);
    }
    sem_init(&g_bindSem, 0, 0);
    Logf(INFO, "Begin bind func, socket fd = %d\n", socketfd);
    int32_t ret = Bind(socketfd, qosInfo, qosCount, &g_listener);
    if (ret < 0) {
        printf("Bind failed, socket fd = %d\n", socketfd);
        sem_destroy(&g_bindSem);
        return;
    }
    Logf(INFO, "Bind func success, socket fd = %d\n", socketfd);
    printf("Bind finish, socket fd = %d\n", socketfd);
    sem_destroy(&g_bindSem);
}

void T_BindAsync(void)
{
    printf("\n>>>Choose use async bind function\n");
    int32_t socketfd = GetInputNumber("Please input socket fd id:");
    int32_t isCustomQosInfo = GetInputNumber("Use custom qos info ? 0 - default (QOS_TYPE_MIN_BW=10*1024*1024, "
                                             "QOS_TYPE_MAX_WAIT_TIMEOUT=4000, QOS_TYPE_MIN_LATENCY=2000), 1 - custom :");
    QosTV qosInfoDefault[] = {
        {.qos = QOS_TYPE_MIN_BW,            .value = 10 * 1024 * 1024},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT, .value = 4000            },
        { .qos = QOS_TYPE_MIN_LATENCY,      .value = 2000            },
    };
    int32_t qosCount = 0;
    int32_t values[7];
    if (isCustomQosInfo == 1) {
        values[0] = GetInputNumber("Please input QOS_TYPE_MIN_BW value:");
        values[1] = GetInputNumber("Please input QOS_TYPE_MAX_WAIT_TIMEOUT value:");
        values[2] = GetInputNumber("Please input QOS_TYPE_MIN_LATENCY value:");
        values[3] = GetInputNumber("Please input QOS_TYPE_MAX_BUFFER value:");
        values[4] = GetInputNumber("Please input QOS_TYPE_FIRST_PACKAGE value:");
        values[5] = GetInputNumber("Please input QOS_TYPE_MAX_IDLE_TIMEOUT value:");
        values[6] = GetInputNumber("Please input QOS_TYPE_TRANS_RELIABILITY value:");
    }
    QosTV qosInfoCustorm[] = {
        {.qos = QOS_TYPE_MIN_BW,             .value = values[0]},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT,  .value = values[1]},
        { .qos = QOS_TYPE_MIN_LATENCY,       .value = values[2]},
        { .qos = QOS_TYPE_MAX_BUFFER,        .value = values[3]},
        { .qos = QOS_TYPE_FIRST_PACKAGE,     .value = values[4]},
        { .qos = QOS_TYPE_MAX_IDLE_TIMEOUT,  .value = values[5]},
        { .qos = QOS_TYPE_TRANS_RELIABILITY, .value = values[6]},
    };
    QosTV *qosInfo;
    if (isCustomQosInfo) {
        qosInfo = &qosInfoCustorm[0];
        qosCount = sizeof(qosInfoCustorm) / sizeof(qosInfoCustorm[0]);
    } else {
        qosInfo = &qosInfoDefault[0];
        qosCount = sizeof(qosInfoDefault) / sizeof(qosInfoDefault[0]);
    }
    bindFlag = true;
    sem_init(&g_bindSem, 0, 0);
    Logf(INFO, "Begin async bind func, socket fd = %d\n", socketfd);
    int32_t ret = BindAsync(socketfd, qosInfo, qosCount, &g_listener);
    if (ret < 0) {
        printf("Async bind failed, socket fd = %d\n", socketfd);
        sem_destroy(&g_bindSem);
        return;
    }
    Logf(INFO, "Begin async bind finish, begin wait call back, socket fd = %d\n", socketfd);
    sem_wait(&g_bindSem);
    printf("Async bind finish, socket fd = %d\n", socketfd);
    sem_destroy(&g_bindSem);
    bindFlag = false;
}

void T_Listen(void) {
    printf("\n>>>Choose use listen function\n");
    int32_t socketfd = GetInputNumber("Please input socket fd id:");
    int32_t isCustomQosInfo = GetInputNumber("Use custom qos info ? 0 - default (QOS_TYPE_MIN_BW=10*1024*1024, "
                                             "QOS_TYPE_MAX_WAIT_TIMEOUT=4000, QOS_TYPE_MIN_LATENCY=2000), 1 - custom :");
    QosTV qosInfoDefault[] = {
        {.qos = QOS_TYPE_MIN_BW,            .value = 10 * 1024 * 1024},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT, .value = 4000            },
        { .qos = QOS_TYPE_MIN_LATENCY,      .value = 2000            },
    };
    int32_t qosCount = 0;
    int32_t values[7];
    if (isCustomQosInfo == 1) {
        values[0] = GetInputNumber("Please input QOS_TYPE_MIN_BW value:");
        values[1] = GetInputNumber("Please input QOS_TYPE_MAX_WAIT_TIMEOUT value:");
        values[2] = GetInputNumber("Please input QOS_TYPE_MIN_LATENCY value:");
        values[3] = GetInputNumber("Please input QOS_TYPE_MAX_BUFFER value:");
        values[4] = GetInputNumber("Please input QOS_TYPE_FIRST_PACKAGE value:");
        values[5] = GetInputNumber("Please input QOS_TYPE_MAX_IDLE_TIMEOUT value:");
        values[6] = GetInputNumber("Please input QOS_TYPE_TRANS_RELIABILITY value:");
    }
    QosTV qosInfoCustorm[] = {
        {.qos = QOS_TYPE_MIN_BW,             .value = values[0]},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT,  .value = values[1]},
        { .qos = QOS_TYPE_MIN_LATENCY,       .value = values[2]},
        { .qos = QOS_TYPE_MAX_BUFFER,        .value = values[3]},
        { .qos = QOS_TYPE_FIRST_PACKAGE,     .value = values[4]},
        { .qos = QOS_TYPE_MAX_IDLE_TIMEOUT,  .value = values[5]},
        { .qos = QOS_TYPE_TRANS_RELIABILITY, .value = values[6]},
    };
    QosTV *qosInfo;
    if (isCustomQosInfo) {
        qosInfo = &qosInfoCustorm[0];
        qosCount = sizeof(qosInfoCustorm) / sizeof(qosInfoCustorm[0]);
    } else {
        qosInfo = &qosInfoDefault[0];
        qosCount = sizeof(qosInfoDefault) / sizeof(qosInfoDefault[0]);
    }

    Logf(INFO, "Begin listen func, socket fd = %d\n", socketfd);
    int32_t ret = Listen(socketfd, qosInfo, qosCount, &g_listener);
    if (ret < 0) {
        printf("listen failed, socket fd = %d\n", socketfd);
        return;
    }
    printf("listen finish, socket fd = %d\n", socketfd);
}

void T_Shutdown(void) {
    int32_t socket;
    socket = GetInputNumber("Please input socket Id:");
    Shutdown(socket);
    printf("Shutdown finish\n");
}

void T_SendMessageQos(void)
{    
    int32_t socket;
    char data[DATA_SIZE];
    socket = GetInputNumber("Please input socket Id:");
    GetInputString("Please input data to SendMessage:", data, DATA_SIZE);
    int32_t ret = SendMessage(socket, data, strlen(data) + 1);
    if (ret != 0) {
        printf("SendMessage failed, ret = %d\n", ret);
    } else {
        printf("SendMessage success\n");
    }
}

void T_SendBytesQos(void)
{    
    int32_t socket;
    char data[DATA_SIZE];
    socket = GetInputNumber("Please input socket Id:");
    GetInputString("Please input data to SendBytes:", data, DATA_SIZE);
    int32_t ret = SendBytes(socket, data, strlen(data) + 1);
    if (ret != 0) {
        printf("SendBytes failed, ret = %d\n", ret);
    } else {
        printf("SendBytes success\n");
    }
}

void T_SendStreamQos(void)
{
    int32_t socket;
    char data[DATA_SIZE];
    StreamData d1 = {0};
    socket = GetInputNumber("Please input socket Id:");
    GetInputString("Please input data to SendStream:", data, DATA_SIZE);
    d1.buf = data;
    d1.bufLen = strlen(data) + 1;
    StreamData d2 = {0};
    StreamFrameInfo tmpf = {0};

    int32_t ret = SendStream(socket, &d1, &d2, &tmpf);
    if (ret != 0) {
        printf("SendStream failed, ret = %d\n", ret);
    } else {
        printf("SendStream success\n");
    }
}

void T_SendFileQos(void)
{
    int32_t socket;
    const char *sfileList[1] = { NULL };
    char filePath[DATA_SIZE] = {0};
    socket = GetInputNumber("Please input session Id:");
    GetInputString("Please input file path to SendFile:", filePath, DATA_SIZE);
    sfileList[0] = filePath;
    int32_t ret = SendFile(socket, sfileList, NULL, 1);
    if (ret != 0) {
        printf("SendFile failed, ret = %d\n", ret);
    } else {
        printf("SendFile success\n");
    }
}

void T_EvaluateQos(void) {
    char networkId[NETWORK_ID_BUF_LEN];
    GetInputString("Please input peeer network Id:", networkId, NETWORK_ID_BUF_LEN);
    int32_t dataType =
        GetInputNumber("Please input socket data type(1 - TYPE_MESSAGE, 2 - TYPE_BYTES, 3 - TYPE_FILE, 4 - "
                       "TYPE_RAW_STREAM, 5 - TYPE_VIDEO_STREAM, 6 - TYPE_AUDIO_STREAM, 7 - TYPE_SLICE_STREAM):");
    int32_t isCustomQosInfo = GetInputNumber("Use custom qos info ? 0 - default (QOS_TYPE_MIN_BW=10*1024*1024, "
                                             "QOS_TYPE_MAX_WAIT_TIMEOUT=4000, QOS_TYPE_MIN_LATENCY=2000), 1 - custom :");
    QosTV qosInfoDefault[] = {
        {.qos = QOS_TYPE_MIN_BW,            .value = 10 * 1024 * 1024},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT, .value = 4000            },
        { .qos = QOS_TYPE_MIN_LATENCY,      .value = 2000            },
    };
    int32_t qosCount = 0;
    int32_t values[7];
    if (isCustomQosInfo == 1) {
        values[0] = GetInputNumber("Please input QOS_TYPE_MIN_BW value:");
        values[1] = GetInputNumber("Please input QOS_TYPE_MAX_WAIT_TIMEOUT value:");
        values[2] = GetInputNumber("Please input QOS_TYPE_MIN_LATENCY value:");
        values[3] = GetInputNumber("Please input QOS_TYPE_MAX_BUFFER value:");
        values[4] = GetInputNumber("Please input QOS_TYPE_FIRST_PACKAGE value:");
        values[5] = GetInputNumber("Please input QOS_TYPE_MAX_IDLE_TIMEOUT value:");
        values[6] = GetInputNumber("Please input QOS_TYPE_TRANS_RELIABILITY value:");
    }
    QosTV qosInfoCustorm[] = {
        {.qos = QOS_TYPE_MIN_BW,             .value = values[0]},
        { .qos = QOS_TYPE_MAX_WAIT_TIMEOUT,  .value = values[1]},
        { .qos = QOS_TYPE_MIN_LATENCY,       .value = values[2]},
        { .qos = QOS_TYPE_MAX_BUFFER,        .value = values[3]},
        { .qos = QOS_TYPE_FIRST_PACKAGE,     .value = values[4]},
        { .qos = QOS_TYPE_MAX_IDLE_TIMEOUT,  .value = values[5]},
        { .qos = QOS_TYPE_TRANS_RELIABILITY, .value = values[6]},
    };
    QosTV *qosInfo;
    if (isCustomQosInfo) {
        qosInfo = &qosInfoCustorm[0];
        qosCount = sizeof(qosInfoCustorm) / sizeof(qosInfoCustorm[0]);
    } else {
        qosInfo = &qosInfoDefault[0];
        qosCount = sizeof(qosInfoDefault) / sizeof(qosInfoDefault[0]);
    }
    Logf(INFO, "Begin evaluateQos func.\n");
    int32_t ret = EvaluateQos(networkId, dataType, qosInfo, qosCount);
    if (ret < 0) {
        printf("evaluateQos failed.\n");
        return;
    }
    printf("evaluateQos finish.\n");
}