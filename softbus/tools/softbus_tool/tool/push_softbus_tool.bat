::**********************************************
:: 说明：
:: 1）将softbus_tool推进连接的所有设备中
:: 2）推入设备的/system/bin 目录下
:: 3）给工具添加执行权限
::
:: 使用：直接执行softbus_tool，然后按提示操作
::**********************************************

@echo off
echo 当前使用的命令是hdc，如果与您的环境不符，请修改脚本
set hdc=hdc

for /f %%i in ('%hdc% list targets') do (
	%hdc% -t %%i shell mount -o rw,remount /
	%hdc% -t %%i file send softbus_tool /system/bin
	%hdc% -t %%i shell chmod +x /system/bin/softbus_tool
)

pause