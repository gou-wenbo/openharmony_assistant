# 说明文档

## 一、 工具说明

此工具用于做软总线接口测试，可以通过命令行的方式调用软总线的接口并传入指定参数，可以在不依赖上层业务的使用来构造各种场景



##  二、编译说明

1. 要随代码编译工具，需要将其加入GN编译链中：

```bash
# 修改gn文件：  foundation\communication\dsoftbus\core\BUILD.gn
} else {
  group("softbus_server") {
    # 在deps里面把softbus_tool的依赖加进来
    deps = [ "frame:softbus_server", "../tests/softbus_tool:softbus_tool" ]
  }
}
```

2. 将**code**目录下的源码放到gn中指定的目录下 `foundation\communication\dsoftbus\tests\`

3. 编译软总线模块：`./build.sh --product-name rk3568 --ccache --build-target dsoftbus`

4. 工具路径：`out\rk3568\communication\dsoftbus\`

## 三、 使用说明

当前在**tool**目录下已经预置一版(20231213)，如果需要更新，本地编译后，替换掉该工具，然后执行脚本**push_softbus_tool.bat**

脚本会把工具推到system/bin目录下，并添加执行权限，所以直接shell登录后，执行**softbus_tool**命令即可