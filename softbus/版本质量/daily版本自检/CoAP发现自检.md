# CoAP发现daily版本自检

## 11月自检结果

| 序号 | 时间 | 版本地址 | UT用例 | 工具-主动发现 | 工具-主动发布 |
| ---- | ---- | -------- | ------ | ------------- | ------------- |
|1|2023/11/3|http://ci.openharmony.cn/workbench/cicd/dailybuild/detail/component|DiscNstackxAdapterTest失败  DiscBleTest无效用例|功能OK|功能OK|
|2|2023/11/6|http://ci.openharmony.cn/workbench/cicd/dailybuild/detail/component|DiscNstackxAdapterTest失败  DiscBleTest无效用例|功能OK|功能OK|
|3|2023/11/7|http://ci.openharmony.cn/workbench/cicd/dailybuild/detail/component|DiscNstackxAdapterTest失败  DiscBleTest无效用例|功能OK|功能OK|
|4|2023/11/8|http://ci.openharmony.cn/workbench/cicd/dailybuild/detail/component|DiscBleTest无效用例|功能OK|功能OK|
|5|2023/11/9|http://ci.openharmony.cn/workbench/cicd/dailybuild/detail/component|DiscBleTest无效用例|功能OK|功能OK|
|6|2023/11/10|http://ci.openharmony.cn/workbench/cicd/dailybuild/detail/component|DiscBleTest无效用例|功能OK|功能OK|

