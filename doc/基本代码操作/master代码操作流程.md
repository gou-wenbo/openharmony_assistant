# master分支操作流程
```bash
repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
repo sync -c
# 下载二进制文件
repo forall -c 'git lfs pull'
# 下载编译工具包
bash build/prebuilts_download.sh
# 代码全编
./build.sh --product-name rk3568 --ccache
# 只编软总线
./build.sh --product-name rk3568 --ccache --build-target dsoftbus --fast-rebuild
# 测试用例编译
./build.sh --product-name rk3568 --ccache --build-target dsoftbus_test --fast-rebuild
```